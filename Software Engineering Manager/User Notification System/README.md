# User Notification System

## Project Scenario
We are a Geospatial Insurtech company providing near real-time insights and reports to our clients on major global catastrophies.

You are tasked with leading a team to strategise and outline the development plan for implementing a User Notification System within our platform, resembling the functionality of Facebook's notification system.

Take time to understand our company and our field of work by consulting all public sources of data available to you.

## Project Requirements

1. **Proposal**
   - Develop a comprehensive proposal detailing the scope, objectives, deliverables, and timeline for implementing the User Notification System.
   - Our current Technology stack is: Python FastAPI for the backend and Typescript ReactJS for the frontend.  

2. **System Architecture Design**
   - Design a high-level architecture diagram that can be presented to non-technical stakeholders.
   - Design a low-level architecture diagram considering scalability, real-time updates, and compatibility with our existing platform.

3. **Implementation Plan**
   - Create a detailed implementation plan encompassing team allocation, technology considerations, API integrations, and an execution roadmap.

4. **Documentation**
   - Prepare comprehensive documentation explaining the design rationale, development approach, and considerations for future enhancements.

## Submission Guidelines
- Submit your proposal, system architecture design, implementation plan, and detailed documentation in a cohesive format, such as a PDF or a presentation.

## Assessment Criteria
- Alignment of proposal with project objectives and feasible timelines.
- Clarity and coherence of the system architecture design, focusing on scalability and real-time functionality.
- Detailed and structured implementation plan.
- Clear and informative documentation addressing key aspects of the project.